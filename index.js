// console.log("asdasd")

// JSON Objects - transfering data
/*
	Syntax:
		{
			"propertyA" : "valueA"
			"propertyB" : "valueB"
		}
*/

// examples:
// {
// 	"city" : "Quezon City",
// 	"province" : "Metro Manil",
// 	"country" : "Philippines"
// }

// // JSON Arrays
// "cities" : [
// 	{
// 		"city" : "Quezon City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	},
// 	{
// 		"city" : "Makati City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines" 
// 	},
// 	{
// 		"city" : "Manila City",
// 		"province" : "Metro Manila",
// 		"country" : "Philippines"
// 	}
// ]

// JSON methods

let BatchesArr = [
	{
		batchName: 'Batch 182'
	},
	{
		batchName: 'Batch 183'
	}
]
// "stringify" - will convert Javascript objects into a string
console.log('Result from stringify method:');
console.log(JSON.stringify(BatchesArr));

let data = JSON.stringify({
	name: 'Eren',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})
console.log(data)

// Stringify method with variables

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which is do you live in?'),
	country: prompt('Which country does your city belongs to?')
};

let otherData = JSON.stringify({
	firstNAme: firstName,
	lastName: lastName,
	age: age,
	address: address
});
console.log(otherData);

// Converting stringified JSON into JS objects
	// JSON.parse()

let batchesJSON = `[
	{
		"batchName" : "Batch 125"
	},	
	{
		"batchName" : "Batch 126"
	
	}
]`
console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name" : "Mikasa",
	"age" : 16,
	"address" : {
		"city" : "Tokyo",
		"country" : "Japan"
	}
}`
console.log(JSON.parse(stringifiedObject));